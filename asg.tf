resource "aws_autoscaling_group" "main_asg" {
  name              = "main-asg"
  min_size          = 1
  desired_capacity  = 1
  max_size          = 2
  target_group_arns = [aws_lb_target_group.main_alb_target_group.arn]

  health_check_type = "ELB"


  //availability_zones = var.availability_zone_wordpress
  vpc_zone_identifier = [
    aws_subnet.subnet1.id,
    aws_subnet.subnet2.id,
  ]

  launch_template {
    id      = aws_launch_template.launch_template.id
    version = "$Latest"

  }
}

resource "aws_launch_template" "launch_template" {
  name_prefix = "ec2-"
  image_id    = local.ami
  key_name      = aws_key_pair.ssh_key.key_name

  instance_type          = local.instance_type
  vpc_security_group_ids = [aws_security_group.ec2_sg.id]

  depends_on = [aws_db_instance.ec2_database]
  user_data = base64encode("${templatefile("${path.module}/init-ec2.sh", {
    DATABASE_NAME     = local.db_name
    DATABASE_USERNAME = local.db_username
    DATABASE_PASSWORD = local.db_password
    DATABASE_ENDPOINT = aws_db_instance.ec2_database.address
  })}")

}

