resource "aws_subnet" "public_subnet1" {
  vpc_id                  = aws_vpc.main_vpc.id
  map_public_ip_on_launch = true
  cidr_block              = local.public_subnet1
  availability_zone       = local.availability_zone[0]

  tags = {
    Name = "public-subnet1"
  }

}


resource "aws_subnet" "public_subnet2" {
  vpc_id                  = aws_vpc.main_vpc.id
  map_public_ip_on_launch = true
  cidr_block              = local.public_subnet2
  availability_zone       = local.availability_zone[1]

  tags = {
    Name = "public-subnet2"
  }

}