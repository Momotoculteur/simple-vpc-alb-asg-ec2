resource "aws_route_table" "private_routetable1" {
  vpc_id = aws_vpc.main_vpc.id
}

resource "aws_route_table" "private_routetable2" {
  vpc_id = aws_vpc.main_vpc.id
}

resource "aws_route_table_association" "private_subnet_1" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.private_routetable1.id
}

resource "aws_route_table_association" "private_subnet_2" {
  subnet_id      = aws_subnet.subnet2.id
  route_table_id = aws_route_table.private_routetable2.id
}

resource "aws_route" "nat_route1_sub1" {
  route_table_id         = aws_route_table.private_routetable1.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat_sub1.id
}

resource "aws_route" "nat_route1_sub2" {
  route_table_id         = aws_route_table.private_routetable2.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat_sub1.id
}
