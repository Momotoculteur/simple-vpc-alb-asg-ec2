
resource "aws_eip" "nat_eip_sub1" {
  domain = "vpc"
}


resource "aws_nat_gateway" "nat_sub1" {
  allocation_id = aws_eip.nat_eip_sub1.id
  subnet_id     = aws_subnet.public_subnet1.id
  tags = {
    Name = "Nat-sub1"
  }
}


resource "aws_eip" "nat_eip_sub2" {
  domain = "vpc"
}


resource "aws_nat_gateway" "nat_sub2" {
  allocation_id = aws_eip.nat_eip_sub2.id
  subnet_id     = aws_subnet.public_subnet2.id
  tags = {
    Name = "Nat-sub2"
  }
}
