resource "aws_subnet" "subnet1" {
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = local.private_subnet1
  availability_zone = local.availability_zone[0]
  tags = {
    Name = "private-subnet1"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = local.private_subnet2
  availability_zone = local.availability_zone[1]
  tags = {
    Name = "private-subnet2"
  }
}


