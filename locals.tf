locals {
  cidr_block      = "10.1.0.0/16"
  private_subnet1 = "10.1.0.0/24"
  private_subnet2 = "10.1.1.0/24"
  public_subnet1  = "10.1.2.0/24"
  public_subnet2  = "10.1.3.0/24"

  availability_zone = [
    "eu-west-3a",
    "eu-west-3c"
  ]

  ami             = "ami-03f12ae727bb56d85"
  instance_type   = "t2.micro"
  ssh_pubkey_path = "/Users/b.maurice/momotoculteur/gitlab/simple-vpc-alb-asg-ec2/projet-perso-aws.pub"

  db_name = "basiquedb"
  db_username = "admin"
  db_password = "adminadmin"

}
