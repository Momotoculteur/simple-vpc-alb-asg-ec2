
resource "aws_db_instance" "ec2_database" {
  allocated_storage      = 10
  username               = local.db_username
  password               = local.db_password
  db_name                = local.db_name
  instance_class         = "db.t2.micro"
  db_subnet_group_name   = aws_db_subnet_group.ec2_database_subnet.name
  vpc_security_group_ids = [aws_security_group.database_sg.id]
  engine                 = "mysql"
  skip_final_snapshot = true
}

resource "aws_db_subnet_group" "ec2_database_subnet" {
  subnet_ids = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]
}


output "database_endpoint_output" {
  value = aws_db_instance.ec2_database.address
}