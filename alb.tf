resource "aws_lb" "main_alb" {
  name               = "main-alb"
  load_balancer_type = "application"


  subnets = [
    aws_subnet.public_subnet1.id,
    aws_subnet.public_subnet2.id

  ]

  security_groups = [aws_security_group.alb_sg.id]
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.main_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.main_alb_target_group.arn
  }
}

resource "aws_lb_target_group" "main_alb_target_group" {
  name     = "main-alb-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main_vpc.id


  health_check {
    enabled  = true
    port     = 80
    #interval = 5
    #timeout  = 2
    protocol = "HTTP"
    path     = "/"
    matcher  = "200-499"
  }

}

resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  autoscaling_group_name = aws_autoscaling_group.main_asg.id
  lb_target_group_arn    = aws_lb_target_group.main_alb_target_group.arn
}

