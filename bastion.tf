resource "aws_instance" "bastion" {
  #ami           = local.ami

  ami = data.aws_ami.amazon-linux-2.id
  instance_type = local.instance_type
  key_name      = aws_key_pair.ssh_key.key_name
  //iam_instance_profile        = aws_iam_instance_profile.session-manager.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.bastion_sg.id]
  subnet_id                   = aws_subnet.public_subnet1.id
  tags = {
    Name = "Bastion"
  }
  depends_on = [aws_db_instance.ec2_database]
  user_data_base64 = base64encode("${templatefile("${path.module}/init-ec2.sh", {
    DATABASE_NAME = local.db_name
    DATABASE_USERNAME = local.db_username
    DATABASE_PASSWORD = local.db_password
    DATABASE_ENDPOINT = aws_db_instance.ec2_database.address
  })}")
}



data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}
