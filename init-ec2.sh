#!/bin/bash

sudo yum update -y
sudo yum install httpd -y
sudo yum install php -y
sudo yum install php-bcmath php-intl php-zip -y
sudo yum install php-mysqli -y
sudo service httpd start
sudo chkconfig httpd on
cd /var/www/html/

sudo yum install wget -y
sudo wget https://wordpress.org/latest.tar.gz
sudo tar -xzf latest.tar.gz
sudo mv wordpress/* .
sudo rm -rf wordpress latest.tar.gz

sudo mv wp-config-sample.php wp-config.php

sed -i 's/database_name_here/${DATABASE_NAME}/' wp-config.php
sed -i 's/username_here/${DATABASE_USERNAME}/' ./wp-config.php
sed -i 's/password_here/${DATABASE_PASSWORD}/' ./wp-config.php
sed -i 's/localhost/${DATABASE_ENDPOINT}/' ./wp-config.php

sudo systemctl restart httpd